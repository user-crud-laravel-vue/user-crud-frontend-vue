import axios from "axios";

const resource = "users";

export default {
  get(config) {
    return axios.get(`/api/${resource}`, config);
  },

  getUser(id) {
    return axios.get(`/api/${resource}/${id}`);
  },

  create(data) {
    return axios.post(`/api/${resource}`, data);
  },

  update(id, data) {
    return axios.put(`/api/${resource}/${id}`, data);
  },

  delete(id) {
    return axios.delete(`/api/${resource}/${id}`);
  }
};
