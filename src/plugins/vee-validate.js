import { extend } from "vee-validate";
import { email, required, max, min } from "vee-validate/dist/rules";

extend("email", email);

extend("min", min);

extend("required", {
  ...required,
  message: "This {_field_} is required"
});

extend("max", {
  ...max,
  message: "The {_field_} field may not be greater than {length} characters"
});
