import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/users"
  },
  {
    path: "/users",
    name: "users.index",
    component: () =>
      import(/* webpackChunkName: "usersList" */ "../views/User/UserIndex")
  },
  {
    path: "/users/create",
    name: "users.create",
    component: () =>
      import(
        /* webpackChunkName: "UserCreateOrUpdate" */ "../views/User/UserCreateOrUpdate"
      )
  },
  {
    path: "/users/:id",
    props: true,
    name: "users.show",
    component: () =>
      import(
        /* webpackChunkName: "UserCreateOrUpdate" */ "../views/User/UserCreateOrUpdate"
      )
  },
  {
    path: "/users/:id/edit",
    props: true,
    name: "users.edit",
    component: () =>
      import(
        /* webpackChunkName: "UserCreateOrUpdate" */ "../views/User/UserCreateOrUpdate"
      )
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
